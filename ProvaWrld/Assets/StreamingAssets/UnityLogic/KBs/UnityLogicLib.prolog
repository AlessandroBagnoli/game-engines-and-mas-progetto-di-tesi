:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/LindaLibrary.prolog").

:- set_prolog_flag(unknown,fail). % closed world assumption

:- op(497,fx,belief). % prefix for belief definition

:- op(498,xfy,&&). % operator used to logically separate events from conditions
:- op(499,xfy,=>). % operator used to logically separate conditions from actions

:- op(500,fx,act).  % prefix used to call an internal method
:- op(500,fx,cr).  % prefix used to start a coroutine

:- op(500,fx,@). % prefix used to specify a reference

:- op(450,xfy,at). % operator used to specify the location of the tuple to release or to search
    
%==================REFERENCE================%
set_ref(R) :- 
    retract(ref(_)),
    assert(ref(R))
    ;
    assert(ref(R)).

get_ref(Ref) :-
    ref(Ref) 
    ; 
    set_ref($this),
    ref(Ref).
    
init :- 
    set_ref($this).
    
%==================COROUTINES===============%
get_coroutine_task(A) :-
    (A = (@Ref,Name,Task), !,
        call_method(Ref,Name,Task))
    ;
    (A = (Name,Task), !,
        get_ref(Ref),
        call_method(Ref,Name,Task)).

%==================ACT======================%
act A :-
    (A = (@Ref,M,Ret), !,
        call_method(Ref,M,Ret))
    ;
    (A = (@Ref,M), !,
        call_method(Ref,M,Ret),
        Ret \= false)
    ;
    (A = (M,Ret), !,
        get_ref(Ref),
        call_method(Ref,M,Ret))
    ;
    (A = M, !,
        get_ref(Ref),
        call_method(Ref,M,Ret),
        Ret \= false).
    
%==================TASK=====================%   
extract_task(A) :-
    (A = [use_artifact(Ref,Action)|N], !, /*Usa l'artefatto*/
        use_artifact(Ref,Action,Ret),
        append(Ret,N,Res),
        set_active_task(Res))
    ;
    (A = [cr (@Ref,M)|N], !, /*Esegue la coroutine*/
        set_active_task((@Ref,M,N)))
    ;
    (A = [cr M|N], !,
        set_active_task((M,N)))
    ;
	(A = [out(M)|N], !,
		manage_out_primitive(M,N))
	;
	(A = [in(M)|N], !,
		manage_in_primitive(M,N))
	;
	(A = [rd(M)|N], !,
		manage_rd_primitive(M,N))
	;
    (A = [M|N], /*Esegue qualunque azione prolog che non sia cr, use_artifact, out, in o rd*/
        M, 
        set_active_task((N))
		;
		/current_desire/current:D,
		del_desire(D)
	).
		
set_active_task(A) :-  
    (A = (@Ref,Name,Cont), !, 
        del_active_task,
        get_coroutine_task((@Ref,Name,Task)),
        assert(/active_task/task:Task),
        assert(/active_task/cont:Cont))
    ;
    (A = (Name,Cont), !,
        del_active_task,
        get_coroutine_task((Name,Task)),
        assert(/active_task/task:Task),
        assert(/active_task/cont:Cont))
    ;
    (A = (Cont), !,
        del_active_task,
        assert(/active_task/task:null),
        assert(/active_task/cont:Cont)).
    
del_active_task :-
    assert(/active_task/task:null),
    assert(/active_task/cont:null).
    
task_completed :- 
    /active_task/cont:Cont,
    del_active_task,
    extract_task(Cont).

manage_out_primitive(M,N) :-
	M = C at R,
	Ref is $'LindaCoordination.Linda4SpatialTuples',
	(
		R = region(L, Ext), !,
		(
			L = coord(Lat, Long, Name),
			set_active_task((@Ref, 'SendMessageToCoordinates'(Lat, Long, Name, Ext, C), N))
			;
			L = here,
			call_method(Ref, 'SendMessageInCurrentRegion'($this, Ext, C), _),
			set_active_task((N))
			;
			L = me,
			call_method(Ref, 'SendMessageToRegionAroundMe'($me, Ext, C), _),
			set_active_task((N))
			;
			string(L),
			call_method(Ref, 'SendMessageToSpatialEntity'(L, Ext, C), _),
			set_active_task((N))
			;
			atom(L),
			set_active_task((@Ref, 'SendMessageToAddress'(L, Ext, C), N))
		)
		;
		R = here, !,
		call_method(Ref, 'SendMessageInCurrentRegion'($this, square(-1), C), _),
		set_active_task((N))
		;
		R = me, !,
		call_method(Ref, 'SendMessageToRegionAroundMe'($me, square(1), C), _),
		set_active_task((N))
		;
		string(R), !,
		call_method(Ref, 'SendMessageToSpatialEntity'(R, square(-1), C), _),
		set_active_task((N))
	).	

manage_in_primitive(M,N) :-
	M = C at R,
	Ref is $'LindaCoordination.Linda4SpatialTuples',
	(
		R = region(L, Ext), !,
		(
			L = coord(Lat, Long),
			set_active_task((@Ref, 'RetrieveMessageFromRegionsAroundCoordinates'(Ext, C, Lat, Long, $this), N))
			;
			L = here,
			set_active_task((@Ref, 'RetrieveMessageFromCurrentRegion'(Ext, C, $this), N))
			;
			L = me,
			set_active_task((@Ref, 'RetrieveMessageFromRegionsAroundMe'(Ext, C, $this), N))
			;
			string(L),
			set_active_task((@Ref, 'RetrieveMessageFromSpatialEntity'(L, Ext, C, $this), N))
			;
			atom(L),
			set_active_task((@Ref, 'RetrieveMessageFromAddress'(L, Ext, C, $this), N))
		)
		;
		R = here, !,
		set_active_task((@Ref, 'RetrieveMessageFromCurrentRegion'(circle(0), C, $this), N))
		;
		R = me, !,
		set_active_task((@Ref, 'RetrieveMessageFromRegionsAroundMe'(circle(1), C, $this), N))
		;
		string(R), !,
		set_active_task((@Ref, 'RetrieveMessageFromSpatialEntity'(R, circle(0), C, $this), N))
	).
	
manage_rd_primitive(M,N) :-
	M = C at R, !, 
	Ref is $'LindaCoordination.Linda4SpatialTuples',
	(
		R = region(L, Ext), !,
		(
			L = coord(Lat, Long),
			set_active_task((@Ref, 'AskMessageFromRegionsAroundCoordinates'(Ext, C, Lat, Long, $this), N))
			;
			L = here,
			set_active_task((@Ref, 'AskMessageFromCurrentRegion'(Ext, C, $this), N))
			;
			L = me,
			set_active_task((@Ref, 'AskMessageFromRegionsAroundMe'(Ext, C, $this), N))
			;
			string(L),
			set_active_task((@Ref, 'AskMessageFromSpatialEntity'(L, Ext, C, $this), N))
			;
			atom(L),
			set_active_task((@Ref, 'AskMessageFromAddress'(L, Ext, C, $this), N))
		)
		;
		R = here, !,
		set_active_task((@Ref, 'AskMessageFromCurrentRegion'(circle(0), C, $this), N))
		;
		R = me, !,
		set_active_task((@Ref, 'AskMessageFromRegionsAroundMe'(circle(1), C, $this), N))
		;
		string(R), !,
		set_active_task((@Ref, 'AskMessageFromSpatialEntity'(R, circle(0), C, $this), N))
	).