:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

belief counter(0).

desire spreadbreadcrumbs.

add spreadbreadcrumbs && true => [
	belief counter(C),
	out(wasHere(hansel, C) at here),
	Cx is C + 1,
	del_belief(counter(C)),
	add_belief(counter(Cx)),
	add_desire(movealong)
].

add movealong && true => [
	cr movealongfortime(1.5),
	add_desire(spreadbreadcrumbs)
].