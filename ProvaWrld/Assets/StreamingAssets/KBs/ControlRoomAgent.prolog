:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

add manage_warning(Ev, Loc) && true => [
	MyName is $me.name,
	act showtoast(MyName, ": nuovo problema rilevato, ", Ev, " in posizione ", Loc),
	out(warning(Ev) at region(Loc, circle(50))),
	(
		atom(Loc),
		add_desire(control_warning(Loc))
		;
		Loc = coord(Lat, Long, _),
		add_desire(control_warning(coord(Lat, Long)))
	)
].

add control_warning(Loc) && true => [
	MyName is $me.name,
	in(triage(_, _) at region(Loc, circle(50))),
	act showtoast(MyName, ": persona soccorsa in zona ", Loc),
	add_desire(redo(Loc))
].

add redo(Loc) && true => [
	add_desire(control_warning(Loc))
].