:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

desire meeting.

add meeting && true => [
	cr gotoregion("meetingPlace"),
	Name is $me.name,
	Waiting is $'sphere (3)'.name,
	act showtoast(Name, ": sono arrivato in regione, rilascio tupla e attendo ", Waiting),
	out(hereIAm(Name) at here),
	rd(hereIAm(Waiting) at here),
	act showtoast(Name, ": ", Waiting, " arrivato, abbandono la regione"),
	cr gotoregion("escape")
].