:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

desire work.

add work && true => [
	add_desire(rescue)
].

add rescue && true => [
	/* in(warning(injured) at region(me, 1000)), */
	rd(warning(injured) at region(coord(43.998938, 12.644691), circle(300))),
	in(warning(injured) at region(here, circle(1000))),
	cr goto("supermercato"),
	out(warning(injured) at here),
	rd(warning(injured) at here),
	/* rd(prova(babbo) at region("Rescuer", 150)), */
	rd(gesu(bambino) at region('Ospedale di Riccione', square(5))),
	out(pippo(pluto) at "Person"),
	rd(pippo(pluto) at "Person"),
	log("FINE")
].