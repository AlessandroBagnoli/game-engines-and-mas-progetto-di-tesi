:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

desire wait_for_emergency.

add wait_for_emergency && true => [
	ID is $me.name,
	out(rescuer(ID, waiting_emergency) at me),
	in(warning(_) at region(here, circle(600))),
	cr gotoemergencyregion,
	add_desire(rescue)
].

add rescue && true => [
	ID is $me.name,
	act (findpersoninregion, Per),
	Per \= null,
	/* act showtoast(ID, ": Trovata persona nella regione, la raggiungo"), */
	in(rescuer(ID, _) at me),
	out(rescuer(ID, triaging) at me),
	cr gotopersoninregion(Per),
	out(triage(rescuer(ID), status('critical')) at Per),
	add_desire(rescue_again)
].

add rescue_again && true => [
	ID is $me.name,
	act (findpersoninregion, Per),
	Per \= null,
	/* act showtoast(ID, ": Trovata persona nella regione, la raggiungo"), */
	in(rescuer(ID, _) at me),
	out(rescuer(ID, triaging) at me),
	cr gotopersoninregion(Per),
	out(triage(rescuer(ID), status('critical')) at Per),
	add_desire(redo)
].

add redo && true => [
	add_desire(rescue_again)
].

del rescue && true => [
	ID is $me.name,
	act showtoast(ID, ": Nessuno da soccorrere in questa regione"),
	act (findsidekick, Sidekick),
	add_agent_desire(Sidekick, follow(ID)),
	in(rescuer(ID, _) at me),
	add_desire(wait_for_emergency)
].

del rescue_again && true => [
	ID is $me.name,
	act showtoast(ID, ": Nessuno rimasto da soccorrere in questa regione"),
	act (findsidekick, Sidekick),
	add_agent_desire(Sidekick, follow(ID)),
	in(rescuer(ID, _) at me),
	in(sidekickArrived(Sidekick) at here), /*Aspetto sidekick prima di andare avanti, spatial sync*/
	add_desire(wait_for_emergency)
].