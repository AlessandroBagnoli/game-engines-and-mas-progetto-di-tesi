:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

belief breadcrumb(1).

add follow(IDRescuer) && true => [
	/* ID is $me.name, */
	(
		belief rescuerID(IDRescuer)
		;
		add_belief(rescuerID(IDRescuer)) /*,*/
		/* act showtoast(ID, ": richiesto il mio intervento, seguo la scia di breadcrumbs") */
	),
	belief breadcrumb(C),
	rd(wasHere(IDRescuer, C) at region(here, circle(300))),
	Cx is C + 1,
	del_belief(breadcrumb(C)),
	add_belief(breadcrumb(Cx)),
	add_desire(gotobreadcrumb)
].

add gotobreadcrumb && true => [
	cr gotobreadcrumb,
	act (amiinregion, Flag),
	Flag = false,
	belief rescuerID(IDRescuer),
	add_desire(follow(IDRescuer))
].

del gotobreadcrumb && true => [
	Me is $me,
	out(sidekickArrived(Me) at here) /*Spatial Synch*/
].