:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

desire meeting.

add meeting && true => [
	cr gotoregion("meetingPlace"),
	Name is $me.name,
	act showtoast(Name, ": sono arrivato nella regione, rilascio tupla"),
	out(hereIAm(Name) at here)
].