:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

belief toArrive(5).

desire work.

add work && true => [
	cr createregion("meetingPlace"),
	add_desire(check)
].

add check && (belief toArrive(N), N > 0) => [
	in(hereIAm(_) at "meetingPlace"),
	Nx is N - 1,
	del_belief(toArrive(N)),
	add_belief(toArrive(Nx)),
	act showtoast("MeetingControl: agente arrivato in zona"),
	add_desire(repeatcheck),
	stop
].

add check && (belief toArrive(N), N =< 0) => [
	act showtoast("MeetingControl: tutti gli agenti sono arrivati in zona")
].

add repeatcheck && true => [
	add_desire(check)
].