:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

belief counter(0).

desire followpattern.

add followpattern && true => [
	belief counter(C),
	in(wasHere(hansel, C) at region(me, square(70))),
	Cx is C + 1,
	del_belief(counter(C)),
	add_belief(counter(Cx)),
	add_desire(gotobreadcrumb)
].

add gotobreadcrumb && true => [
	cr gotobreadcrumb,
	add_desire(followpattern)
].