:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

belief counter(1).

desire randomize.

add randomize && true => [
	belief counter(C),
	act (getcontrolroomagentref(C), Ref),
	act (getrandomenvironmentevent, Ev),
	act (getrandomenvironmentlocation, Loc),
	add_agent_desire(Ref, manage_warning(Ev, Loc)),
	add_desire(redo)
].

add redo && true => [
	del_belief(counter(C)),
	Cx is C + 1,
	Cx < 5,
	add_belief(counter(Cx)),
	cr waitforseconds(3.0),
	add_desire(randomize)
].