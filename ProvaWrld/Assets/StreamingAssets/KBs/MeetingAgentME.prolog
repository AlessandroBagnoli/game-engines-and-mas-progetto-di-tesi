:- consult("/storage/emulated/0/Android/data/com.alessandrobagnoli.spatialtuples/files/UnityLogic/KBs/UnityLogicAgentAPI.prolog").

desire meeting.

add meeting && true => [
	cr getclosetoregion("mutex_region"),
	in(lock(_) at "mutex_region"),
	Name is $me.name,
	act showtoast(Name, ": tupla 'lock' ottenuta, mi muovo verso la regione"),
	cr gotoregion("mutex_region"),
	act showtoast(Name, ": sono arrivato nella regione, rilascio tupla 'lock' e abbandono la regione"),
	out(lock(0) at "mutex_region"),
	cr gotoregion("escape")
].