﻿using System.Collections;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class Hansel : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private ToastMessage toastManager;

    IEnumerator Start () {
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
            Init(kbPath, kbName);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    public IEnumerator MoveAlongForTime(float timeout)
    {
        float forwardTimeElapsed = 0.0f;
        while (forwardTimeElapsed < timeout)
        {
            forwardTimeElapsed += Time.deltaTime;
            Vector3 movement = Vector3.forward * movementSpeed * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
    }


}
