﻿using System.Collections;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class Follower : Agent
{

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private ToastMessage toastManager;

    IEnumerator Start()
    {
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
            Init(kbPath, kbName);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    public IEnumerator GoToBreadcrumb()
    {
        Vector3 targetPosition = base.TupleFoundIn.transform.position;

        while (Vector3.Distance(targetPosition, gameObject.transform.position) >= 2f)
        {
            Vector3 direction = targetPosition - gameObject.transform.position;
            Vector3 movement = direction * movementSpeed * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
        Destroy(base.TupleFoundIn);
    }
}