﻿using UnityEngine;
using Prolog;

namespace UnityLogic
{
    public abstract class Agent : AbstractLinda
    {
        public GameObject CurrentRegion { get; private set; }
        public GameObject TupleFoundIn { get; set; }

        private bool initialized = false;

        protected void Init(string kbPath, string kbName)
        {
            base.LocalKB = new KnowledgeBase(kbName, gameObject);
            string kbPathFinal = kbPath;
#if UNITY_ANDROID && !UNITY_EDITOR
            var filepath = string.Format("{0}/{1}", Application.persistentDataPath, kbPath + ".prolog");
            kbPathFinal = filepath;
#endif
            base.LocalKB.Consult(kbPathFinal);
            base.LocalKB.IsTrue(new ISOPrologReader("init.").ReadTerm(), this);
            initialized = true;
        }

        public void LateUpdate()
        {
            if (initialized)
            {
                base.LocalKB.IsTrue(new ISOPrologReader("go_on.").ReadTerm(), this);
            }
        }

        public object CheckPlan(object head)
        {
            var full = new LogicVariable("Full");
            try
            {
                base.LocalKB.SolveFor(full, new Structure("check_plan", head, full), this);
            }
            catch (System.Exception)
            {
                return false;
            }

            return full;
        }

        #region BELIEFS

        public bool AddBelief(object belief)
        {
            return base.LocalKB.IsTrue(new ISOPrologReader("add_belief(" + belief + ").").ReadTerm(), this);
        }

        public bool DelBelief(object belief)
        {
            return base.LocalKB.IsTrue(new ISOPrologReader("del_belief(" + belief + ").").ReadTerm(), this);
        }

        #endregion

        #region DESIRES

        public bool AddDesire(object desire)
        {
            return base.LocalKB.IsTrue(new ISOPrologReader("add_desire(" + desire + ").").ReadTerm(), this);
        }

        public bool DelDesire(object desire)
        {
            return base.LocalKB.IsTrue(new ISOPrologReader("del_desire(" + desire + ").").ReadTerm(), this);
        }

        #endregion

        private void OnTriggerEnter(Collider other)
        {
            //Se collido con una regione che non è quella che mi porto dietro, allora la salvo per tenerne traccia
            if (other.gameObject.CompareTag("Region") && !other.gameObject.name.Contains(this.gameObject.name + "region"))
            {
                //print("Sono entrato nella regione " + other.gameObject.name);
                this.CurrentRegion = other.gameObject;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            //Se esco dalla collisione con una regione, imposto current region a null
            if (other.gameObject.CompareTag("Region") && !other.gameObject.name.Contains(this.gameObject.name + "region") && other.gameObject == this.CurrentRegion)
            {
                //print("Sono uscito dalla regione " + other.gameObject.name);
                this.CurrentRegion = null;
            }
        }
    }
}