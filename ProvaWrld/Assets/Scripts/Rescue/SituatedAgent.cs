﻿using LindaCoordination;
using Prolog;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class SituatedAgent : Agent
{

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private ToastMessage toastManager;

    private int counter;
    private GameObject lastEmergencyRegion;

    private IEnumerator Start()
    {
        this.counter = 1;
        this.lastEmergencyRegion = null;
        yield return new WaitForSeconds(2.0f);
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
            Init(kbPath, kbName);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    public IEnumerator GoToEmergencyRegion()
    {
        double startLatitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double startLongitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        double endLatitude = this.TupleFoundIn.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double endLongitude = this.TupleFoundIn.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();

        //Obtain all the directions to arrive to the emergency region
        IList<LatLong> coordinatesList = new List<LatLong>();
        IEnumerator coroutineDirections = LocationUtils.GetDirectionsFromGoogle(startLatitude, startLongitude, endLatitude, endLongitude, (directions) =>
        {
            coordinatesList = directions;
        });
        while (coroutineDirections.MoveNext() || coordinatesList.Count == 0)
        {
            yield return null;
        }

        //Move towards each direction's coordinates
        Linda4SpatialTuples.SendMessageInCurrentRegion(this, new Structure("square", -1), new Structure("wasHere", this.name, counter));
        LatLong lastLocation = new LatLong();
        foreach (LatLong coordinate in coordinatesList)
        {
            this.counter++;
            double latitude = coordinate.GetLatitude();
            double longitude = coordinate.GetLongitude();
            //print("LAT: " + latitude + " LON: " + longitude);
            double altitudeFound = Double.NaN;
            GameObject geographic = null;
            GameObject child = null;
            IEnumerator altitudeEnumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (altitude) =>
            {
                altitudeFound = altitude + 5f;
                string guid = Guid.NewGuid().ToString();
                geographic = new GameObject(guid + "geo");
                GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
                geographicTransform.SetPosition(coordinate);
                child = new GameObject(guid);
                child.transform.SetParent(geographic.transform);
                child.transform.localPosition = new Vector3(0, (float)altitudeFound, 0);
            });
            while (altitudeEnumerator.MoveNext() || altitudeFound == Double.NaN || geographic.transform.position.x == 0 || child.transform.localPosition.y != (float)altitudeFound)
            {
                yield return null;
            }

            Vector3 direction = child.transform.position - gameObject.transform.position;
            while (Vector3.Distance(child.transform.position, gameObject.transform.position) >= 0.5f)
            {
                Vector3 movement = direction * 0.1f * Time.deltaTime;
                gameObject.transform.Translate(movement);
                yield return null;
            }
            Linda4SpatialTuples.SendMessageInCurrentRegion(this, new Structure("square", -1), new Structure("wasHere", this.name, this.counter));
            Destroy(geographic);
            lastLocation = coordinate;
        }

        //Update real coordinates of the agent.
        transform.parent.GetComponent<GeographicTransform>().SetPosition(lastLocation);
        transform.localPosition = new Vector3(0, transform.localPosition.y, 0);
        //print("FINITO");
    }

    public string FindPersonInRegion()
    {
        GameObject[] people = GameObject.FindGameObjectsWithTag("Person");
        foreach (GameObject person in people)
        {
            if (person.name.StartsWith("PersonToRescue" + this.CurrentRegion.name) && !person.GetComponent<PersonToRescue>().Triaged)
            {
                person.GetComponent<PersonToRescue>().Triaged = true;
                return person.name;
            }
        }
        return null;
    }

    public IEnumerator GoToPersonInRegion(string personName)
    {
        GameObject person = GameObject.Find(personName);
        Vector3 direction = person.transform.position - gameObject.transform.position;
        while (Vector3.Distance(person.transform.position, gameObject.transform.position) >= 3.5f)
        {
            Vector3 movement = direction * 0.3f * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
    }

    public void ShowToast(params object[] name)
    {
        string concat = "";
        foreach (object s in name)
        {
            concat += s.ToString();
        }
        this.toastManager.showToastOnUiThread(concat);
    }

    public object FindSideKick()
    {
        string sidekickName = name.Replace("Rescuer", "Sidekick");
        return GameObject.Find(sidekickName);
    }

    public bool AmIInregion()
    {
        if (this.CurrentRegion.transform.localScale.x > 1 && !this.CurrentRegion.Equals(this.lastEmergencyRegion))
        {
            this.lastEmergencyRegion = this.CurrentRegion;
            return true;
        }
        else
        {
            return false;
        }
    }

    public IEnumerator GoToBreadcrumb()
    {
        Vector3 targetPosition = base.TupleFoundIn.transform.position;

        Vector3 direction = targetPosition - transform.position;
        while (Vector3.Distance(targetPosition, transform.position) >= 2f)
        {
            Vector3 movement = direction * 0.2f * Time.deltaTime;
            transform.Translate(movement);
            yield return null;
        }
        //Destroy(base.TupleFoundIn);
    }

}
