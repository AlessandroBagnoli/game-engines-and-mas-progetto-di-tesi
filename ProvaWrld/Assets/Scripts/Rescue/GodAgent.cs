﻿using Prolog;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityLogic;

public class GodAgent : Agent
{

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private ToastMessage toastManager;

    private static readonly System.Random rnd = new System.Random();
    private static readonly Symbol[] environmentEvents = {
        Symbol.Intern("fire"),
        Symbol.Intern("earthquake"),
        Symbol.Intern("tornado")
        //...
    };
    private static IList<object> environmentLocations;

    private IEnumerator Start()
    {
        environmentLocations = new List<object>()
        {
            Symbol.Intern("'angelini viale diaz riccione'"),
            new Structure("coord", 43.99785, 12.65947, "stazione"),
            Symbol.Intern("'Bar City Cafe Riccione'"),
            new Structure("coord", 43.996394, 12.656937, "viale scesa")
            //...
        };
        yield return new WaitForSeconds(2.0f); //waiting 2 seconds before starting everything, just for the map initialization
        Init(kbPath, kbName);
    }

    public object GetControlRoomAgentRef(int num)
    {
        Agent ag = GameObject.Find("ControlRoomAgent" + num).GetComponent<Agent>();
        if (ag != null)
        {
            return ag.gameObject;
        }
        return false;
    }

    public Symbol GetRandomEnvironmentEvent()
    {
        int r = rnd.Next(environmentEvents.Length);
        return environmentEvents[r];
    }

    public object GetRandomEnvironmentLocation()
    {
        int r = rnd.Next(environmentLocations.Count);
        object location = environmentLocations[r];
        environmentLocations.RemoveAt(r);
        return location;
    }

    public IEnumerator WaitForSeconds(float seconds)
    {
        float start = Time.time;
        while (Time.time - start < seconds)
        {
            yield return null;
        }
    }

    public void ShowToast(params object[] name)
    {
        string concat = "";
        foreach (object s in name)
        {
            concat += s.ToString();
        }
        this.toastManager.showToastOnUiThread(concat);
    }
}
