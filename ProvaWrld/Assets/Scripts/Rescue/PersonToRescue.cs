﻿using System;
using System.Collections;
using UnityEngine;
using Wrld.Space;

public class PersonToRescue : MonoBehaviour {

    private string originalName;
    public bool Triaged { get; set; }

    private IEnumerator Start()
    {
        this.originalName = gameObject.name;
        this.Triaged = false;
        yield return new WaitForSeconds(2.0f);
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Region"))
        {
            gameObject.name = this.originalName + other.name + Guid.NewGuid().ToString();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Region"))
        {
            gameObject.name = this.originalName;
        }
    }
}
