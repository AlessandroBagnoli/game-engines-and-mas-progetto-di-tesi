﻿using System.Collections;
using UnityEngine;
using UnityLogic;

public class ControlRoomAgent : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private ToastMessage toastManager;

    private void Start () {
        Init(kbPath, kbName);
    }
	
    public void ShowToast(params object[] name)
    {
        string concat = "";
        foreach (object s in name)
        {
            concat += s.ToString();
        }
        this.toastManager.showToastOnUiThread(concat);
    }
	
}
