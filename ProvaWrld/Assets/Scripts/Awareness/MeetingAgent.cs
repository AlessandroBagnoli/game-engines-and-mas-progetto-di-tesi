﻿using System.Collections;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class MeetingAgent : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private ToastMessage toastManager;

    IEnumerator Start () {
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
            Init(kbPath, kbName);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    public IEnumerator GetCloseToRegion(string regionName)
    {
        GameObject target = GameObject.Find(regionName);
        while (target == null)
        {
            target = GameObject.Find(regionName);
            yield return null;
        }
        while (Vector3.Distance(target.transform.position, gameObject.transform.position) >= target.transform.localScale.x - 8f)
        {
            Vector3 direction = target.transform.position - gameObject.transform.position;
            Vector3 movement = direction * movementSpeed * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
    }

    public IEnumerator GoToRegion(string regionName)
    {
        GameObject target = GameObject.Find(regionName);
        while (target == null)
        {
            target = GameObject.Find(regionName);
            yield return null;
        }
        while (Vector3.Distance(target.transform.position, gameObject.transform.position) >= 5f)
        {
            Vector3 direction = target.transform.position - gameObject.transform.position;
            Vector3 movement = direction * movementSpeed * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
    }

    public void ShowToast(params string[] name)
    {
        string concat = "";
        foreach (string s in name)
        {
            concat += s;
        }
        this.toastManager.showToastOnUiThread(concat);
    }

}
