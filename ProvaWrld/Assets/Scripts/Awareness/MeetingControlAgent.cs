﻿using System.Collections;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class MeetingControlAgent : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private ToastMessage toastManager;

    void Start()
    {
        Init(kbPath, kbName);
    }

    public IEnumerator CreateRegion(string regionName)
    {
        LatLong pointA = LatLong.FromDegrees(40.77237, -73.97547);
        float altitude = 0;
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(pointA.GetLatitude(), pointA.GetLongitude(), (myReturnValue) =>
        {
            altitude = (float)myReturnValue;
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
        Region region = new Region(regionName, new Vector3(0f, altitude, 0f), new Vector3(40, 40, 40), PrimitiveType.Cube, true, Quaternion.identity);


        GameObject regionGo = GameObject.CreatePrimitive(region.Type);

        regionGo.name = region.Name;
        regionGo.tag = "Region";
        regionGo.layer = LayerMask.NameToLayer("Region");
        regionGo.GetComponent<MeshRenderer>().material = Resources.Load("Materials/Transparent") as Material;
        regionGo.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        if (region.Type.Equals(PrimitiveType.Capsule) || region.Type.Equals(PrimitiveType.Cylinder))
        {
            regionGo.transform.rotation = region.Rotation;
        }
        regionGo.GetComponent<Collider>().isTrigger = region.IsTrigger;

        GameObject geographic = new GameObject(regionGo.name + "geo");
        GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
        geographicTransform.SetPosition(pointA);
        regionGo.transform.SetParent(geographic.transform);
        regionGo.transform.localPosition = region.Centre;
        regionGo.transform.localScale = region.Scale;

        SituatedPassiveKB script = regionGo.AddComponent<SituatedPassiveKB>();
        script.path = "";
        script.InitKB();
    }

    public IEnumerator CreateEscapeRegion()
    {
        LatLong pointA = LatLong.FromDegrees(40.774647, -73.972751);
        float altitude = 0;
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(pointA.GetLatitude(), pointA.GetLongitude(), (myReturnValue) =>
        {
            altitude = (float)myReturnValue;
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
        Region region = new Region("escape", new Vector3(0f, altitude, 0f), new Vector3(40, 40, 40), PrimitiveType.Cube, true, Quaternion.identity);


        GameObject regionGo = GameObject.CreatePrimitive(region.Type);

        regionGo.name = region.Name;
        regionGo.tag = "Region";
        regionGo.layer = LayerMask.NameToLayer("Region");
        regionGo.GetComponent<MeshRenderer>().material = Resources.Load("Materials/Transparent") as Material;
        regionGo.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        if (region.Type.Equals(PrimitiveType.Capsule) || region.Type.Equals(PrimitiveType.Cylinder))
        {
            regionGo.transform.rotation = region.Rotation;
        }
        regionGo.GetComponent<Collider>().isTrigger = region.IsTrigger;

        GameObject geographic = new GameObject(regionGo.name + "geo");
        GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
        geographicTransform.SetPosition(pointA);
        regionGo.transform.SetParent(geographic.transform);
        regionGo.transform.localPosition = region.Centre;
        regionGo.transform.localScale = region.Scale;

        SituatedPassiveKB script = regionGo.AddComponent<SituatedPassiveKB>();
        script.path = "";
        script.InitKB();
    }

    public void ShowToast(string message)
    {
        this.toastManager.showToastOnUiThread(message);
    }

}
