﻿using System.Collections;
using UnityEngine;
using Wrld;
using Wrld.Space;

public class GPSLocationService : MonoBehaviour
{
    [SerializeField]
    private GeographicTransform coordinateFrame;
    [SerializeField]
    private Transform sphere;

    private void OnEnable()
    {
        Api.Instance.GeographicApi.RegisterGeographicTransform(coordinateFrame);
    }

    private IEnumerator Start()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            yield break;
        }

        // Start service before querying location
        Input.location.Start(10f, 2f);

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            //print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            LatLong pointA = LatLong.FromDegrees(Input.location.lastData.latitude, Input.location.lastData.longitude);
            float altitude = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(pointA.GetLatitude(), pointA.GetLongitude(), (myReturnValue) =>
            {
                altitude = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }
            //print("ALTITUDINE:" + altitude);
            Api.Instance.CameraApi.AnimateTo(pointA, distanceFromInterest: 0, headingDegrees: 0, tiltDegrees: 45);
            sphere.localPosition = new Vector3(0.0f, altitude, 0.0f); //TODO: nonostante il fix, alcuni luoghi continuano a dare problemi, come ad esempio Milano o Rimini.
            coordinateFrame.SetPosition(pointA);
            //print("Transform di coordinateFrame:" + coordinateFrame.transform.localPosition.x + " " + coordinateFrame.transform.localPosition.y + " " + coordinateFrame.transform.localPosition.z);
        }

        
        yield break;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        Api.Instance.GeographicApi.UnregisterGeographicTransform(coordinateFrame);
        Input.location.Stop();
    }

    private void Update()
    {
        if (Input.location.status == LocationServiceStatus.Running)
        {
            LatLong pointA = LatLong.FromDegrees(Input.location.lastData.latitude, Input.location.lastData.longitude);
            coordinateFrame.SetPosition(pointA);
        }
    }

    /// <summary>
    /// Centers the camera on the current position.
    /// </summary>
    public void CenterCamera()
    {
        Api.Instance.CameraApi.MoveTo(this.coordinateFrame.GetLatLong(), distanceFromInterest: 0);
        //Api.Instance.CameraApi.AnimateTo(this.coordinateFrame.GetLatLong(), distanceFromInterest: 0, headingDegrees: 0, tiltDegrees: 45, transitionDuration: 5, jumpIfFarAway: true); non funziona più l'animate dopo l'aggiornamento
    }

}
