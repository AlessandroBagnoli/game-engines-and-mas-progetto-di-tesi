﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wrld.Space;

public class LocationUtils {

    private static readonly string API_KEY = "AIzaSyA3covdb0mnn5WF8m1oJQncgi_EX_nliaU";

    /// <summary>
    /// Gets the altitude of a point via Google APIs starting from latitude and longitude.
    /// </summary>
    /// <param name="latitude">Latitude of the point.</param>
    /// <param name="longitude">Longitude of the point.</param>
    /// <returns>The altitude.</returns>
    public static IEnumerator GetAltitudeFromGoogle(double latitude, double longitude, Action<double> callback)
    {
        string url = "https://maps.googleapis.com/maps/api/elevation/json?locations=" + latitude + "," + longitude + "&key=" + API_KEY;
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (www.error == null)
        {
            JSONNode j = JSON.Parse(www.text);
            callback(j["results"][0]["elevation"].AsDouble);
        }
        else
        {
            callback(0);
        }
    }

    /// <summary>
    /// Gets the altitude of a point via Open Elevation (OSM) APIs starting from latitude and longitude.
    /// </summary>
    /// <param name="latitude">Latitude of the point.</param>
    /// <param name="longitude">Longitude of the point.</param>
    /// <returns></returns>
    public static IEnumerator GetAltitudeFromOSM(double latitude, double longitude, Action<double> callback)
    {
        string url = "https://api.open-elevation.com/api/v1/lookup?locations=" + latitude + "," + longitude;
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (www.error == null)
        {
            JSONNode j = JSON.Parse(www.text);
            callback(j["results"][0]["elevation"].AsDouble);
        }
        else
        {
            callback(0);
        }
    }

    /// <summary>
    /// Gets the formatted address along with the latitude and longitude via Google APIs starting from a generic address.
    /// </summary>
    /// <param name="address">The address to be geocoded.</param>
    /// <returns>A Tuple data structure containing the formatted address and the LatLong object.</returns>
    public static IEnumerator GetGeocodingFromGoogle(string address, Action<Tuple<string, LatLong>> callback)
    {
        string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + API_KEY;
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (www.error == null)
        {
            JSONNode j = JSON.Parse(www.text);
            string formattedAddress = j["results"][0]["formatted_address"].Value;
            double lat = j["results"][0]["geometry"]["location"]["lat"].AsDouble;
            double lng = j["results"][0]["geometry"]["location"]["lng"].AsDouble;
            callback(Tuple.Create(formattedAddress, new LatLong(lat, lng)));
        }
        else
        {
            callback(null);
        }
    }

    /// <summary>
    /// Gets the formatted address along with the latitude and longitude via Nominatim (OSM) APIs starting from a generic address.
    /// </summary>
    /// <param name="address">The address to be geocoded.</param>
    /// <returns>A Tuple data structure containing the formatted address and the LatLong object.</returns>
    public static IEnumerator GetGeocodingFromOSM(string address, Action<Tuple<string, LatLong>> callback)
    {
        string url = "https://nominatim.openstreetmap.org/search?format=json&q=" + address;
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (www.error == null)
        {
            JSONNode j = JSON.Parse(www.text);
            string formattedAddress = j[0]["display_name"].Value;
            double lat = j[0]["lat"].AsDouble;
            double lng = j[0]["lon"].AsDouble;
            callback(Tuple.Create(formattedAddress, new LatLong(lat, lng)));
        }
        else
        {
            callback(null);
        }
    }

    public static IEnumerator GetDirectionsFromGoogle(double startLatitude, double startLongitude, double endLatitude, double endLongitude, Action<IList<LatLong>> callback)
    {
        string url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + startLatitude +"," + startLongitude + "&destination=" + endLatitude + "," + endLongitude + "&mode=walking&key=" + API_KEY;
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            yield return null;
        }
        if (www.error == null)
        {
            IList<LatLong> directions = new List<LatLong>();
            JSONNode j = JSON.Parse(www.text);
            JSONNode steps = j["routes"][0]["legs"][0]["steps"];
            for (int key = 0; key < steps.Count; key++)
            {
                double latitude = steps[key]["end_location"]["lat"].AsDouble;
                double longitude = steps[key]["end_location"]["lng"].AsDouble;
                directions.Add(new LatLong(latitude, longitude));
            }
            callback(directions);
        }
        else
        {
            callback(null);
        }
    }
}
