﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{

    public void ChangeScene(string sceneName)
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
    }

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
