﻿using System.IO;
using UnityEngine;

public class AppStartup : MonoBehaviour {

    private static readonly string streamingUnityLogic = "UnityLogic/KBs";
    private static readonly string streamingKBs = "KBs";
    private static readonly string prologExtension = "*.prolog";

    private void Awake()
    {
        print("Initializing Prolog resources...");
        BetterStreamingAssets.Initialize();

        //getting prolog file paths from within the apk package
        string[] pathsUnityLogic = BetterStreamingAssets.GetFiles(streamingUnityLogic, prologExtension);
        string[] pathsKBs = BetterStreamingAssets.GetFiles(streamingKBs, prologExtension);

        //creating directories where to put prolog files
        Directory.CreateDirectory(string.Format("{0}/{1}", Application.persistentDataPath, streamingUnityLogic));
        Directory.CreateDirectory(string.Format("{0}/{1}", Application.persistentDataPath, streamingKBs));

        //creating prolog files in unity persistent data path, where I can access them
        foreach (string path in pathsUnityLogic)
        {
            string content = BetterStreamingAssets.ReadAllText(path);
            var filepath = string.Format("{0}/{1}", Application.persistentDataPath, path);
            File.WriteAllText(filepath, content);
        }
        foreach (string path in pathsKBs)
        {
            string content = BetterStreamingAssets.ReadAllText(path);
            var filepath = string.Format("{0}/{1}", Application.persistentDataPath, path);
            File.WriteAllText(filepath, content);
        }

        //I do not need this gameobject anymore, destroying it
        Destroy(this.gameObject);
    }
}
