﻿using UnityEngine;
using Wrld;
using Wrld.Space;

public class CameraManagement : MonoBehaviour {

    private void OnEnable()
    {
        SetUpCamera();
    }

    private void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    private void SetUpCamera()
    {
        var startLocation = LatLong.FromDegrees(43.997185, 12.657227);
        Api.Instance.CameraApi.MoveTo(startLocation, distanceFromInterest: 900, headingDegrees: 45, tiltDegrees: 15);
    }

}
