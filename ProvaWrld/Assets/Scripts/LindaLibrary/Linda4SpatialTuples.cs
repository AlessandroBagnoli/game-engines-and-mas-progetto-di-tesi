﻿using UnityEngine;
using System;
using Wrld.Space;
using Linda;
using UnityLogic;
using System.Collections;
using Prolog;

namespace LindaCoordination
{
    public static class Linda4SpatialTuples
    {
        #region OUT primitive
        //'out(tuple at region((Lat, Long, Name), Ext))'
        //TODO allo stato attuale creo regioni senza controllare che ce ne siano altre nello stesso punto con la stessa estensione
        /// <summary>
        /// Releases a tuple in specific spatial coordinates.
        /// </summary>
        /// <param name="latitude">Latitude where the tuple needs to be released.</param>
        /// <param name="longitude">Longitude where the tuple needs to be released.</param>
        /// <param name="regionName">The name of the <seealso cref="Region"/> created to handle the tuple.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/>.</param>
        /// <returns></returns>
        public static IEnumerator SendMessageToCoordinates(double latitude, double longitude, string regionName, Structure extension, Structure tuple)
        {
            LatLong pointA = LatLong.FromDegrees(latitude, longitude);
            float alt = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
            {
                alt = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float ext = Convert.ToSingle(extension.Argument(0));
            Region region = new Region(regionName, new Vector3(0f, alt, 0f), new Vector3(ext, ext < 20 ? ext : 20, ext), shape, false, Quaternion.identity);


            GameObject regionGo = GameObject.CreatePrimitive(region.Type);

            regionGo.name = region.Name;
            regionGo.tag = "Region";
            regionGo.layer = LayerMask.NameToLayer("Region");
            regionGo.GetComponent<MeshRenderer>().material = Resources.Load("Materials/Transparent") as Material;
            regionGo.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            if (region.Type.Equals(PrimitiveType.Capsule) || region.Type.Equals(PrimitiveType.Cylinder))
            {
                regionGo.transform.rotation = region.Rotation;
            }
            regionGo.GetComponent<Collider>().isTrigger = region.IsTrigger;

            GameObject geographic = new GameObject(regionGo.name + "geo");
            GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
            geographicTransform.SetPosition(pointA);
            regionGo.transform.SetParent(geographic.transform);
            regionGo.transform.localPosition = region.Centre;
            regionGo.transform.localScale = region.Scale;

            SituatedPassiveKB script = regionGo.AddComponent<SituatedPassiveKB>();
            script.path = "";
            script.InitKB();
            yield return LindaLibrary.Linda_OUT(tuple.ToString(), script.LocalKB);
        }

        //'out(tuple at region("name",ext))'
        /// <summary>
        /// Releases a tuple in a spatial entity. Can be a <seealso cref="Region"/> or any other gameobject on the scene.
        /// Does nothing if the entity is not found.
        /// </summary>
        /// <param name="name">The name of the spatial entity.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/></param>
        /// <returns></returns>
        public static bool SendMessageToSpatialEntity(string name, Structure extension, Structure tuple)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float ext = Convert.ToSingle(extension.Argument(0));

            GameObject gameObject = GameObject.Find(name);
            if (gameObject != null)
            {
                //se trovo una regione col nome passato
                if (gameObject.tag == "Region")
                {
                    if (Math.Abs(gameObject.transform.localScale.x - ext) <= 0.7 || ext < 0) //se la regione ha la stessa estensione ext o se provengo da una forma implicita ci faccio la out senza ricreare la region
                    {
                        return LindaCoordinationUtilities.Tell(tuple.ToString(), gameObject);
                    }
                    else //altrimenti devo creare la region e farci la out
                    {
                        Region region = new Region(Guid.NewGuid().ToString(), gameObject.transform.position, new Vector3(ext, ext < 20 ? ext : 20, ext), shape, false, Quaternion.identity);
                        return LindaCoordinationUtilities.SendMessageToRegion(tuple.ToString(), region);
                    }
                }
                else //altrimenti dovro creare una regione da portare appresso alla entità 
                {
                    return SendMessageToRegionAroundMe(gameObject, ext < 0 ? new Structure(extension.Functor.Name, 1) : extension, tuple);
                }
            }
            else
            {
                Debug.Log("l'entità con nome " + name + " non è stata trovata, la tupla è andata persa");
                return false;
            }
        }

        //out at region(me,ext)
        /// <summary>
        /// Releases a tuple on any gameobject which is not a <seealso cref="Region"/>, creating a new one of those able to move along with the gameobject.
        /// </summary>
        /// <param name="entity">The gameobject where to attach the tuple.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/>.</param>
        /// <returns></returns>
        public static bool SendMessageToRegionAroundMe(GameObject entity, Structure extension, Structure tuple)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float ext = Convert.ToSingle(extension.Argument(0));

            //se c'è gia la regione che mi porto appresso che ha la stessa estensione passata, non la ricrei, fai soltanto la out su di essa.
            foreach (GameObject regiongo in GameObject.FindGameObjectsWithTag("Region"))
            {
                if (regiongo.name.Contains(entity.name + "region") && Math.Abs(regiongo.transform.localScale.x - ext) <= 0.7)
                {
                    //Debug.Log("La regione che mi porto appresso esiste gia ed ha la stessa estensione passata come parametro, scrivo la tupla su di essa");
                    return LindaCoordinationUtilities.Tell(tuple.ToString(), regiongo);
                }
            }

            //altrimenti ne crei una nuova
            Region reg = new Region(entity.name + "region" + Guid.NewGuid().ToString(), new Vector3(0f, 0f, 0f), new Vector3(ext, ext < 20 ? ext : 20, ext), shape, false, Quaternion.identity);
            GameObject region = GameObject.CreatePrimitive(reg.Type);

            region.name = reg.Name;
            region.tag = "Region";
            region.layer = LayerMask.NameToLayer("Region");
            region.GetComponent<MeshRenderer>().material = Resources.Load("Materials/Transparent") as Material;
            region.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            if (reg.Type.Equals(PrimitiveType.Capsule) || reg.Type.Equals(PrimitiveType.Cylinder))
            {
                region.transform.rotation = reg.Rotation;
            }
            region.GetComponent<Collider>().isTrigger = reg.IsTrigger;
            region.AddComponent<Rigidbody>().isKinematic = true;

            region.transform.localScale = reg.Scale;
            region.transform.SetParent(entity.transform);
            region.transform.localPosition = reg.Centre;

            SituatedPassiveKB script = region.AddComponent<SituatedPassiveKB>();
            script.path = "";
            script.InitKB();
            return LindaLibrary.Linda_OUT(tuple.ToString(), script.LocalKB);
        }

        //out at region(here,ext)
        /// <summary>
        /// Releases a tuple in the current <seealso cref="Region"/> where the <paramref name="agent"/> is located, or creates a new one if 
        /// <paramref name="agent"/> is not located in any <seealso cref="Region"/>.
        /// </summary>
        /// <param name="agent">The agent.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/>.</param>
        /// <returns></returns>
        public static bool SendMessageInCurrentRegion(Agent agent, Structure extension, Structure tuple)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float ext = Convert.ToSingle(extension.Argument(0));
            if (ext < 0)
            {
                if (agent.CurrentRegion != null)
                {
                    //Debug.Log("IMPLICIT SEI IN UNA REGION, FACCIO OUT SU DI LEI");
                    return LindaCoordinationUtilities.SendMessageToRegionName(tuple.ToString(), agent.CurrentRegion.name) == ReturnTypeKB.True ? true : false;
                }
                else
                {
                    //Debug.Log("IMPLICIT NON SEI IN UNA REGION, LA DEVO CREARE E FARE OUT SU DI LEI");
                    Vector3 regionCenter = agent.transform.position;
                    Region region = new Region(Guid.NewGuid().ToString(), regionCenter, new Vector3(1, 1, 1), shape, false, Quaternion.identity);
                    return LindaCoordinationUtilities.SendMessageToRegion(tuple.ToString(), region);
                }
            }
            else
            {
                if (agent.CurrentRegion != null)
                {
                    if (agent.CurrentRegion.transform.localScale.x == ext) //se mi trovo in una regione e l'estensione è uguale alla estensione della regione in cui mi trovo, faccio out sulla regione in cui mi trovo
                    {
                        //Debug.Log(agent.gameObject.name + ": SEI IN UNA REGION CON STESSA ESTENSIONE, FACCIO OUT SU DI LEI");
                        return LindaCoordinationUtilities.SendMessageToRegionName(tuple.ToString(), agent.CurrentRegion.name) == ReturnTypeKB.True ? true : false;
                    }
                    else
                    {
                        //Debug.Log(agent.gameObject.name + ": SEI IN UNA REGION CON DIVERSA ESTENSIONE, NE CREO UNA NUOVA"); //se mi trovo in una regione e l'estensione è diversa dalla estensione della regione in cui mi trovo, crea una nuova regione con centro la regione attuale ed estensione passata come parametro
                        Vector3 regionCenter = agent.transform.position;
                        Region region = new Region(Guid.NewGuid().ToString(), regionCenter, new Vector3(ext, ext < 20 ? ext : 20, ext), shape, false, Quaternion.identity);
                        return LindaCoordinationUtilities.SendMessageToRegion(tuple.ToString(), region);
                    }
                }
                else //se non mi trovo in nessuna regione, la creo da 0 con centro la mia posizione
                {
                    //Debug.Log("NON SEI IN UNA REGION, LA DEVO CREARE E FARE OUT SU DI LEI");
                    Vector3 regionCenter = agent.transform.position;
                    Region region = new Region(Guid.NewGuid().ToString(), regionCenter, new Vector3(ext, ext < 20 ? ext : 20, ext), shape, false, Quaternion.identity);
                    return LindaCoordinationUtilities.SendMessageToRegion(tuple.ToString(), region);
                }
            }
        }

        //out at region('address', ext)
        /// <summary>
        /// Releases a tuple in a given spatial address - e.g.: 'Ospedale Bufalini di Cesena'.
        /// </summary>
        /// <param name="address">The spatial address.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/>.</param>
        /// <returns></returns>
        public static IEnumerator SendMessageToAddress(object address, Structure extension, Structure tuple)
        {
            Tuple<string, LatLong> addressTuple = null;
            IEnumerator enumeratorLatLong = LocationUtils.GetGeocodingFromGoogle(address.ToString(), (myReturnValue) =>
            {
                addressTuple = myReturnValue;
            });
            while (enumeratorLatLong.MoveNext())
            {
                yield return null;
            }
            string formattedAddress = addressTuple.Item1;
            double lat = addressTuple.Item2.GetLatitude();
            double lng = addressTuple.Item2.GetLongitude();
            if (SendMessageToSpatialEntity(formattedAddress, extension, tuple)) //controllo che ci sia gia una regione con lo stesso nome
            {
                yield return true; //se c'è ci pensa gia SendMessageToSpatialEntity a fare la out, sulla stessa regione se hanno la stessa ext, altrimenti su una nuova con diversa ext
            }
            else
            {
                IEnumerator enumerator = SendMessageToCoordinates(lat, lng, formattedAddress, extension, tuple); //altrimenti creo una nuova regione a quelle coordinate e col nome giusto
                while (enumerator.MoveNext())
                {
                    yield return null;
                }
            }
        }
        #endregion

        #region IN primitive
        //in at region("name",ext) SOSPENSIVA
        /// <summary>
        /// Retrieves a matching tuple (deleting it) within a range starting from the specified spatial entity's position. 
        /// Suspends the agent until the spatial entity and/or the tuple are found.
        /// </summary>
        /// <param name="name">The name of the spatial entity.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator RetrieveMessageFromSpatialEntity(string name, Structure extension, Structure tuple, Agent agent)
        {
            GameObject entity = GameObject.Find(name);
            while (entity == null)
            {
                entity = GameObject.Find(name);
                yield return null;
            }

            Vector3 location = entity.transform.position;
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0)) + entity.transform.localScale.x;

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Retrieve(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //in at region(me,ext) SOSPENSIVA
        /// <summary>
        /// Retrieves a matching tuple (deleting it) within a range starting from the agent's current position. 
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator RetrieveMessageFromRegionsAroundMe(Structure extension, Structure tuple, Agent agent)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));

            bool found = false;
            Vector3 location;
            GameObject[] regionsInRange;
            while (!found)
            {
                location = agent.transform.position;
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Retrieve(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //in at region((lat, long), ext) SOSPENSIVA
        /// <summary>
        /// Retrieves a matching tuple (deleting it) within a range starting from latitude and longitude coordinates. 
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="latitude">The latitude where the search takes place.</param>
        /// <param name="longitude">The longitude where the search takes place.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator RetrieveMessageFromRegionsAroundCoordinates(Structure extension, Structure tuple, double latitude, double longitude, Agent agent)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            float altitude = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
            {
                altitude = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }

            //Traduco lat e long (coordinate real world) in coordinate Unity
            GameObject geographic = new GameObject(Guid.NewGuid().ToString());
            GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
            geographicTransform.SetPosition(new LatLong(latitude, longitude));
            GameObject inner = new GameObject(Guid.NewGuid().ToString());
            inner.transform.SetParent(geographic.transform);
            inner.transform.localPosition = new Vector3(0f, altitude, 0f);

            //Le coordinate unity vengono ottenute con qualche ritardo, le attendo
            Vector3 location = inner.transform.position;
            while (location.x == 0 && location.z == 0)
            {
                //Debug.Log("SEMPRE 0");
                location = inner.transform.position;
                yield return null;
            }
            //GameObject.Destroy(geographic);

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log("TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Retrieve(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //in at region(here, ext) SOSPENSIVA
        /// <summary>
        /// Retrieves a matching tuple (deleting it) within a range starting from the agent's current <seealso cref="Region"/>. If the agent is in no <seealso cref="Region"/>, the search is made starting from
        /// the position of the agent itself.
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator RetrieveMessageFromCurrentRegion(Structure extension, Structure tuple, Agent agent)
        {
            Vector3 location;
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            GameObject currentRegion = agent.CurrentRegion;

            //se mi trovo in una regione, cerco considerando come centro il centro della regione e come estensione l'estensione della regione + l'estensione passata
            if (currentRegion != null)
            {
                //Debug.Log(agent.name + ": SONO IN UNA REGIONE");
                location = currentRegion.transform.position;
                radius += currentRegion.transform.localScale.x;
            }
            else //altrimenti cerco considerando come centro il centro dell'agente e come estensione l'estensione passata
            {
                //Debug.Log(agent.name + ": NON SONO IN UNA REGIONE");
                location = agent.transform.position;
            }

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Retrieve(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //in at region('indirizzo', ext) SOSPENSIVA
        /// <summary>
        /// Retrieves a matching tuple (deleting it) within a range starting from a generic address, e.g.: 'Ospedale Bufalini di Cesena'.
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="address">The address where to search the tuple.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator RetrieveMessageFromAddress(object address, Structure extension, Structure tuple, Agent agent)
        {
            LatLong latLng = new LatLong(0, 0);
            IEnumerator enumeratorLatLong = LocationUtils.GetGeocodingFromGoogle(address.ToString(), (myReturnValue) =>
            {
                latLng = myReturnValue.Item2;
            });
            while (enumeratorLatLong.MoveNext())
            {
                yield return null;
            }
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            float altitude = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latLng.GetLatitude(), latLng.GetLongitude(), (myReturnValue) =>
            {
                altitude = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }

            //Traduco lat e long (coordinate real world) in coordinate Unity
            GameObject geographic = new GameObject(Guid.NewGuid().ToString());
            GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
            geographicTransform.SetPosition(latLng);
            GameObject inner = new GameObject(Guid.NewGuid().ToString());
            inner.transform.SetParent(geographic.transform);
            inner.transform.localPosition = new Vector3(0f, altitude, 0f);

            //Le coordinate unity vengono ottenute con qualche ritardo, le attendo
            Vector3 location = inner.transform.position;
            while (location.x == 0 && location.z == 0)
            {
                location = inner.transform.position;
                yield return null;
            }
            //GameObject.Destroy(geographic);

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log("TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Retrieve(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }
        #endregion

        #region RD primitive
        //rd at region("name",ext) SOSPENSIVA
        /// <summary>
        /// Asks for a matching tuple within a range starting from the specified spatial entity's position. 
        /// Suspends the agent until the spatial entity and/or the tuple are found.
        /// </summary>
        /// <param name="name">The name of the spatial entity.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator AskMessageFromSpatialEntity(string name, Structure extension, Structure tuple, Agent agent)
        {
            GameObject entity = GameObject.Find(name);
            while (entity == null)
            {
                entity = GameObject.Find(name);
                yield return null;
            }

            Vector3 location = entity.transform.position;
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0)) + entity.transform.localScale.x;

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(agent.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Ask(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //rd at region(me,ext) SOSPENSIVA
        /// <summary>
        /// Asks for a matching tuple within a range starting from the agent's current position. 
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator AskMessageFromRegionsAroundMe(Structure extension, Structure tuple, Agent agent)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));

            bool found = false;
            Vector3 location;
            GameObject[] regionsInRange;
            while (!found)
            {
                location = agent.transform.position;
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Ask(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //rd at region((lat, long), ext) SOSPENSIVA
        /// <summary>
        /// Asks for a matching tuple within a range starting from latitude and longitude coordinates. 
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="latitude">The latitude where the search takes place.</param>
        /// <param name="longitude">The longitude where the search takes place.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator AskMessageFromRegionsAroundCoordinates(Structure extension, Structure tuple, double latitude, double longitude, Agent agent)
        {
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            float altitude = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
            {
                altitude = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }

            //Traduco lat e long (coordinate real world) in coordinate Unity
            GameObject geographic = new GameObject(Guid.NewGuid().ToString());
            GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
            geographicTransform.SetPosition(new LatLong(latitude, longitude));
            GameObject inner = new GameObject(Guid.NewGuid().ToString());
            inner.transform.SetParent(geographic.transform);
            inner.transform.localPosition = new Vector3(0f, altitude, 0f);

            //Le coordinate unity vengono ottenute con qualche ritardo, le attendo
            Vector3 location = inner.transform.position;
            while (location.x == 0 && location.z == 0)
            {
                //Debug.Log("SEMPRE 0");
                location = inner.transform.position;
                yield return null;
            }
            //GameObject.Destroy(geographic);

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log("TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Ask(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //rd at region(here, ext) SOSPENSIVA
        /// <summary>
        /// Asks for a matching tuple within a range starting from the agent's current <seealso cref="Region"/>. If the agent is in no <seealso cref="Region"/>, the search is made starting from
        /// the position of the agent itself.
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator AskMessageFromCurrentRegion(Structure extension, Structure tuple, Agent agent)
        {
            Vector3 location;
            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            GameObject currentRegion = agent.CurrentRegion;

            //se mi trovo in una regione, cerco considerando come centro il centro della regione e come estensione l'estensione della regione + l'estensione passata
            if (currentRegion != null)
            {
                //Debug.Log(agent.name + ": SONO IN UNA REGIONE");
                location = currentRegion.transform.position;
                radius += currentRegion.transform.localScale.x;
            }
            else //altrimenti cerco considerando come centro il centro dell'agente e come estensione l'estensione passata
            {
                //Debug.Log(agent.name + ": NON SONO IN UNA REGIONE");
                location = agent.transform.position;
            }

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Ask(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }

        //rd at region('indirizzo', ext) SOSPENSIVA
        /// <summary>
        /// Asks for a matching tuple within a range starting from a generic address, e.g.: 'Ospedale Bufalini di Cesena'.
        /// Suspends the agent until the tuple is found.
        /// </summary>
        /// <param name="address">The address where to search the tuple.</param>
        /// <param name="extension">The extension <seealso cref="Structure"/>.</param>
        /// <param name="tuple">The tuple <seealso cref="Structure"/> to match.</param>
        /// <param name="agent">The agent performing the operation.</param>
        /// <returns></returns>
        public static IEnumerator AskMessageFromAddress(object address, Structure extension, Structure tuple, Agent agent)
        {
            LatLong latLng = new LatLong(0, 0);
            IEnumerator enumeratorLatLong = LocationUtils.GetGeocodingFromGoogle(address.ToString(), (myReturnValue) =>
            {
                latLng = myReturnValue.Item2;
            });
            while (enumeratorLatLong.MoveNext())
            {
                yield return null;
            }

            PrimitiveType shape = FromStringToPrimitiveType(extension.Functor.Name);
            float radius = Convert.ToSingle(extension.Argument(0));
            float altitude = 0;
            IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latLng.GetLatitude(), latLng.GetLongitude(), (myReturnValue) =>
            {
                altitude = (float)myReturnValue;
            });
            while (enumerator.MoveNext())
            {
                yield return null;
            }

            //Traduco lat e long (coordinate real world) in coordinate Unity
            GameObject geographic = new GameObject(Guid.NewGuid().ToString());
            GeographicTransform geographicTransform = geographic.AddComponent<GeographicTransform>();
            geographicTransform.SetPosition(latLng);
            GameObject inner = new GameObject(Guid.NewGuid().ToString());
            inner.transform.SetParent(geographic.transform);
            inner.transform.localPosition = new Vector3(0f, altitude, 0f);

            //Le coordinate unity vengono ottenute con qualche ritardo, le attendo
            Vector3 location = inner.transform.position;
            while (location.x == 0 && location.z == 0)
            {
                location = inner.transform.position;
                yield return null;
            }
            //GameObject.Destroy(geographic);

            bool found = false;
            GameObject[] regionsInRange;
            while (!found)
            {
                regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, shape, radius, 50);
                yield return null;
                foreach (GameObject region in regionsInRange)
                {
                    //Debug.Log("TROVATA REGIONE " + region.name);
                    if (LindaCoordinationUtilities.Ask(tuple.ToString(), region))
                    {
                        found = true;
                        agent.TupleFoundIn = region;
                        break;
                    }
                    yield return null;
                }
                yield return null;
            }
        }
        #endregion

        private static PrimitiveType FromStringToPrimitiveType(string shape)
        {
            switch (shape)
            {
                case "circle":
                    return PrimitiveType.Sphere;
                case "square":
                    return PrimitiveType.Cube;
                default:
                    return PrimitiveType.Cube;
            }
        }

        #region prove non sospensive
        ////rd at region("name",ext) NON SOSPENSIVA
        //public static bool AskMessageToSpatialEntity(string name, object extension, Structure tuple) //TODO extension non utilizzato
        //{
        //    GameObject gameObject = GameObject.Find(name);
        //    if (gameObject != null)
        //    {
        //        return LindaCoordinationUtilities.Ask(tuple.ToString(), gameObject);
        //    }
        //    else
        //    {
        //        Debug.Log("l'entità con nome " + name + " non è stata trovata, non è stato possibile cercare la tupla");
        //        return false;
        //    }
        //}

        ////in at region("name",ext) NON SOSPENSIVA
        //public static bool RetrieveMessageFromSpatialEntity(string name, object extension, Structure tuple)
        //{
        //    GameObject gameObject = GameObject.Find(name);
        //    if (gameObject != null)
        //    {
        //        return LindaCoordinationUtilities.Retrieve(tuple.ToString(), gameObject);
        //    }
        //    else
        //    {
        //        Debug.Log("l'entità con nome " + name + " non è stata trovata, non è stato possibile cercare la tupla");
        //        return false;
        //    }
        //}
        #endregion

        #region prove
        //public static async Task<bool> RetrieveMessageFromSpatialEntitySusp(object name, object extension, object tuple, AbstractLinda me)
        //{
        //    GameObject entity = GameObject.Find(name.ToString());
        //    if (entity != null)
        //    {
        //        //Debug.Log("PRIMA DI AWAIT");
        //        bool x = await LindaCoordinationUtilities.RetrieveSuspend(tuple.ToString(), entity, me);
        //        //Debug.Log("DOPO DI AWAIT");
        //        return x;
        //    }
        //    else
        //    {
        //        Debug.Log("l'entità con nome " + name.ToString() + " non è stata trovata, non è stato possibile cercare la tupla");
        //        return false;
        //    }
        //}

        //public static async Task<bool> RetrieveMessageFromRegionsAroundMe(object extension, object tuple, GameObject me)
        //{
        //    float radius = Convert.ToSingle(extension);
        //    Vector3 location = new Vector3(me.transform.position.x, me.transform.position.y, me.transform.position.z);

        //    GameObject[] regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, radius, 10);

        //    //await Task.Run(() => STA ROBA BLOCCA
        //    //{
        //    //    while (regionsInRange.Length == 0)
        //    //    {
        //    //        regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, radius, 10);
        //    //    }
        //    //});

        //    //while (regionsInRange.Length == 0) QUESTA PURE, COME FACCIO AD ASPETTARE LA CREAZIONE DI REGIONI NELLA MIA AREA?
        //    //{
        //    //    Debug.Log("NESSUNA REGIONE TROVATA");
        //    //    regionsInRange = LindaCoordinationUtilities.GetSituatedObjectsFromArea(location, radius, 10);
        //    //}

        //    List<Task<bool>> taskList = new List<Task<bool>>();
        //    foreach (GameObject region in regionsInRange)
        //    {
        //        if (region != null)
        //        {
        //            Debug.Log(me.name + ": TROVATA REGIONE " + region.name);
        //            //Task<bool> x = RetrieveMessageFromSpatialEntitySusp(region.name, radius, tuple, me.GetComponent<AbstractLinda>());
        //            Task<bool> x = LindaCoordinationUtilities.RetrieveSuspend(tuple.ToString(), region.name, me.GetComponent<AbstractLinda>());
        //            taskList.Add(x);
        //        }
        //    }

        //    Task<bool> completedTask = await Task.WhenAny(taskList);
        //    //me.GetComponent<AbstractLinda>().Suspended = true;
        //    bool ct = await completedTask;
        //    me.GetComponent<AbstractLinda>().Suspended = false;
        //    return ct;
        //}



        //NON FUNZIONA
        //public static IEnumerator RetrieveMessageFromSpatialEntitySusp(object name, object extension, object tuple, AbstractLinda me)
        //{
        //    GameObject entity = GameObject.Find(name.ToString());
        //    if (entity != null)
        //    {
        //        Debug.Log("PRIMA DI AWAIT");
        //        yield return LindaCoordinationUtilities.RetrieveSuspend_Coroutine(tuple.ToString(), entity, me);
        //        Debug.Log("DOPO DI AWAIT");
        //    }
        //    else
        //    {
        //        Debug.Log("l'entità con nome " + name.ToString() + " non è stata trovata, non è stato possibile cercare la tupla");
        //        yield return null;
        //    }
        //}
        #endregion

    }
}