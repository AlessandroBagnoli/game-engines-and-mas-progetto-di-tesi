﻿using System.Collections;
using System.Timers;
using UnityLogic;

public class Person : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    void Start () {
        Init(kbPath, kbName);
    }
	
	public void ReportInjury()
    {
        print("Person: I'm injured, creating region...");
    }

    public IEnumerator DoStuff()
    {
        int secondsToWait = new System.Random().Next(5, 10);
        Timer timer = new Timer(1000);
        timer.Start();
        timer.Elapsed += (object sender, ElapsedEventArgs e) => secondsToWait--;
        print("Person: Doing stuff for " + secondsToWait + " seconds...");
        while (secondsToWait >= 0)
        {
            yield return null;
        }
    }
}
