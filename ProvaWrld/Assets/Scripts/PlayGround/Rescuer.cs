﻿using System.Collections;
using System.Timers;
using UnityEngine;
using UnityLogic;
using Wrld.Space;

public class Rescuer : Agent {

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private ToastMessage toastManager;

    IEnumerator Start () {
        double latitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude();
        double longitude = gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude();
        IEnumerator enumerator = LocationUtils.GetAltitudeFromGoogle(latitude, longitude, (myReturnValue) =>
        {
            float altitude = (float)myReturnValue + gameObject.transform.localScale.x;
            gameObject.transform.localPosition = new Vector3(0f, altitude, 0f);
            Init(kbPath, kbName);
        });
        while (enumerator.MoveNext())
        {
            yield return null;
        }
    }

    public IEnumerator GoTo(string region)
    {
        //print("PRIMA: " + gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude() + " " + gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude());
        toastManager.showToastOnUiThread("Mi muovo verso la regione " + region);
        GameObject target = GameObject.Find(region);
        while (Vector3.Distance(target.transform.position, gameObject.transform.position) >= 5f) //fintanto che non mi trovo nella regione, mi sposto verso di essa
        {
            Vector3 direction = target.transform.position - gameObject.transform.position;
            Vector3 movement = direction * movementSpeed * Time.deltaTime;
            gameObject.transform.Translate(movement);
            yield return null;
        }
        //print("DOPO: " + gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLatitude() + " " + gameObject.transform.parent.GetComponent<GeographicTransform>().GetLatLong().GetLongitude());
    }

    public IEnumerator DoStuff()
    {
        int secondsToWait = new System.Random().Next(2, 3);
        Timer timer = new Timer(1000);
        timer.Start();
        timer.Elapsed += (object sender, ElapsedEventArgs e) => secondsToWait--;
        print("Rescuer: Doing stuff for " + secondsToWait + " seconds...");
        while (secondsToWait >= 0)
        {
            yield return null;
        }
    }

}
